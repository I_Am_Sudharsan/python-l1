def ipaddr(n):
    t=64
    v=128
    if(n==0):
        return 0
    elif(n==1):
        return v
    else:
        for i in range(1,n):
            v=v+t
            t=t/2
        return v

inp=input("Enter a net mask : ")

if(inp<8):
    ip=ipaddr(inp)
    print "IP Address is : ",ip,".0.0.0"
elif(inp<16):
    inp=inp-8
    ip = ipaddr(inp)
    print "IP Address is : 255.", ip, ".0.0"
elif(inp<24):
    inp=inp-16
    ip = ipaddr(inp)
    print "IP Address is : 255.255.", ip, ".0"
elif(inp<32):
    inp=inp-24
    ip = ipaddr(inp)
    print "IP Address is : 255.255.255.", ip