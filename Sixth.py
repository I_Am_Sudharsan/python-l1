def maxval(list):
    list.sort()
    length = len(list)
    values = [list[length - 1], list[length - 2]]
    return values

def minval(list):
    list.sort()
    values = [list[0], list[1]]
    return values

maxlist=[]
minlist=[]

list1=[84,54,21,65,39,24,75]
val=maxval(list1)
maxlist=maxlist+val
val=minval(list1)
minlist=minlist+val

list2=[54,54,35,12,75,12,55,4,11,58]
val=maxval(list2)
maxlist=maxlist+val
val=minval(list2)
minlist=minlist+val

list3=[45,54,24,1,12,75,65,36,55]
val=maxval(list3)
maxlist=maxlist+val
val=minval(list3)
minlist=minlist+val

maxavg=0
minavg=0
for i in range(6):
    maxavg=maxlist[i]+maxavg
    minavg=minlist[i]+minavg

maxavg=maxavg/6
minavg=minavg/6

print "The maximum average is : ",maxavg
print "The minimum average is : ",minavg